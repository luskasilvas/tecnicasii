package banksys.account;

import static org.junit.Assert.*;

import org.junit.Test;

import banksys.account.exception.InsufficientFundsException;
import banksys.account.exception.NegativeAmountException;

public class SpecialAccountTest {

	@Test
	public void test() throws NegativeAmountException, InsufficientFundsException {
		SpecialAccount account = new SpecialAccount("123B");
		account.credit(50);
		assertEquals(50, account.getBalance(), 0.0);
		
		account.debit(30);
		assertEquals(20, account.getBalance(), 0.0);
	}

}
