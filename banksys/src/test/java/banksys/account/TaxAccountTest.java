package banksys.account;

import static org.junit.Assert.*;

import org.junit.Test;

import banksys.account.exception.InsufficientFundsException;
import banksys.account.exception.NegativeAmountException;

public class TaxAccountTest {

	@Test
	public void creditAndDebitTest() throws NegativeAmountException, InsufficientFundsException {
		TaxAccount account = new TaxAccount("123B");
		account.credit(50);
		assertEquals(50, account.getBalance(), 0.0);
		
		account.debit(30);
		assertEquals(19.97, account.getBalance(), 0.0);
		
	}

}
