package banksys.bank;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import banksys.account.OrdinaryAccount;
import banksys.account.SavingsAccount;
import banksys.control.BankController;
import banksys.control.exception.BankTransactionException;
import banksys.persistence.AccountVector;
import banksys.persistence.exception.AccountCreationException;

public class TransferTestFRomOrdinaryToSavingsAccount {

	@Test
	public void test() throws AccountCreationException, BankTransactionException {
		
		OrdinaryAccount ordinaryAccount = new OrdinaryAccount("123A");
		SavingsAccount savingsAccount = new SavingsAccount("123B");
		AccountVector accountVector = new AccountVector();
		
		accountVector.create(ordinaryAccount);
		accountVector.create(savingsAccount);
		
		BankController bank = new BankController(accountVector);
		
		bank.doCredit("123A", 100);
		bank.doCredit("123B", 50);
		
		bank.doTransfer("123A", "123B", 50);
		assertEquals(bank.getBalance("123A"), 50, 0.0);
		assertEquals(bank.getBalance("123B"), 100, 0.0);
		
	}

}
